PROJECT_NAME := nodezoo-service-chart
PROJECT_SLUG := n1470/${PROJECT_NAME}
GITLAB_REPO := registry.gitlab.com

.phony: ci_image
ci_image:
	@docker build --build-arg PROJECT_PATH=${PROJECT_SLUG} -t ${GITLAB_REPO}/${PROJECT_SLUG} .
