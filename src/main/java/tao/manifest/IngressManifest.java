package tao.manifest;

import imports.k8s.*;
import org.jetbrains.annotations.NotNull;
import tao.PkgServiceChart;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

public class IngressManifest {

    public interface IngressProps {
        List<IngressRule> rules();
        Map<String, String> annotations();
    }

    public IngressManifest(@NotNull PkgServiceChart chart,
                           @NotNull String id,
                           @NotNull IngressProps ingressManifestProps) {

        final var name = MessageFormat.format("{0}-service-ingress", id);
        final var selector = Map.of("name", name);
        final var objectMeta = ObjectMeta.builder()
                .name(name)
                .labels(selector)
                .annotations(ingressManifestProps.annotations())
                .build();
        final var spec = IngressSpec.builder()
                .rules(ingressManifestProps.rules())
                .build();
        final var ingressProps = KubeIngressProps.builder()
                .metadata(objectMeta)
                .spec(spec)
                .build();
        new KubeIngress(chart, "ingress", ingressProps);
    }

    public static class IngressPropsBuilder {
        public static NginxProps buildNginx(@NotNull String hostName,
                                            @NotNull String serviceName,
                                            int servicePort) {
            return new NginxProps(hostName, buildHttpIngress(serviceName, servicePort));
        }

        public static AlbProps buildAlb(@NotNull String serviceName, int servicePort) {
            return new AlbProps(buildHttpIngress(serviceName, servicePort));
        }

        private static HttpIngressRuleValue buildHttpIngress(@NotNull String serviceName, int servicePort) {
            return HttpIngressRuleValue.builder()
                    .paths(List.of(HttpIngressPath.builder()
                            .path("/")
                            .pathType("Prefix")
                            .backend(IngressBackend.builder()
                                    .service(IngressServiceBackend.builder()
                                            .name(serviceName)
                                            .port(ServiceBackendPort.builder()
                                                    .number(servicePort)
                                                    .build())
                                            .build())
                                    .build())
                            .build()))
                    .build();
        }
    }

    public static class NginxProps implements IngressProps {
        @NotNull String hostName;
        @NotNull HttpIngressRuleValue httpIngress;

        public NginxProps(@NotNull String hostName, @NotNull HttpIngressRuleValue httpIngress) {
            this.hostName = hostName;
            this.httpIngress = httpIngress;
        }

        @Override
        public List<IngressRule> rules() {
            final var rule = IngressRule.builder()
                    .http(this.httpIngress)
                    .host(this.hostName)
                    .build();
            return List.of(rule);
        }

        @Override
        public Map<String, String> annotations() {
            return Map.of("kubernetes.io/ingress.class", "nginx");
        }
    }

    public static class AlbProps implements IngressProps {

        @NotNull HttpIngressRuleValue httpIngress;

        public AlbProps(@NotNull HttpIngressRuleValue httpIngress) {
            this.httpIngress = httpIngress;
        }

        @Override
        public List<IngressRule> rules() {
            final var rule = IngressRule.builder()
                    .http(this.httpIngress)
                    .build();
            return List.of(rule);
        }

        @Override
        public Map<String, String> annotations() {
            return Map.of("kubernetes.io/ingress.class", "alb");
        }
    }
}
