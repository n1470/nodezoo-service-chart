package tao.manifest;

import imports.k8s.*;
import org.jetbrains.annotations.NotNull;
import tao.PkgServiceChart;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

public class ServiceManifest {

    private final KubeService service;
    private final String appSelectorValue;

    public static class Props {
        @NotNull String id;
        int servicePort;
        int targetPort;
        boolean isEks;
        @NotNull String serviceName;

        public Props(int servicePort, 
                     int targetPort, 
                     boolean isEks,
                     @NotNull String id,
                     @NotNull String serviceName) {
            
            this.servicePort = servicePort;
            this.targetPort = targetPort;
            this.isEks = isEks;
            this.serviceName = serviceName;
            this.id = id;
        }
    }

    public ServiceManifest(PkgServiceChart npmServiceChart, Props serviceManifestProps) {
        final var port = ServicePort
                .builder()
                .port(serviceManifestProps.servicePort)
                .targetPort(IntOrString.fromNumber(serviceManifestProps.targetPort))
                .build();
        final var servicePorts = List.of(port);
        final var serviceSpecBuilder = ServiceSpec.builder();
        if (serviceManifestProps.isEks) {
            serviceSpecBuilder.type("NodePort");
        }

        this.appSelectorValue = MessageFormat.format("{0}-service-prod", serviceManifestProps.id);
        final var serviceSpec = serviceSpecBuilder
                .selector(getSelector())
                .ports(servicePorts)
                .build();
        final var serviceProps = KubeServiceProps
                .builder()
                .spec(serviceSpec)
                .metadata(ObjectMeta.builder().name(serviceManifestProps.serviceName).build())
                .build();
        this.service = new KubeService(npmServiceChart, "service", serviceProps);
    }

    @NotNull
    public Map<String, String> getSelector() {
        return Map.of("app", appSelectorValue);
    }

    public KubeService getService() {
        return this.service;
    }
}
