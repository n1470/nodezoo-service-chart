package tao.core;

import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;

public interface AwsGateway {
    AwsCredentialsProvider buildProvider(String profileName);

    String fetchAwsAccountId(AwsCredentialsProvider credentialsProvider);

    String fetchEcrAuthToken(AwsCredentialsProvider credentialsProvider);
}
