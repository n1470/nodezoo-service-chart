package tao;

import org.cdk8s.Chart;
import org.cdk8s.ChartProps;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.constructs.Construct;
import tao.manifest.DeploymentManifest;
import tao.manifest.ImagePullSecretManifest;
import tao.manifest.IngressManifest;
import tao.manifest.ServiceManifest;

import java.text.MessageFormat;

import static tao.manifest.IngressManifest.IngressPropsBuilder;

public class PkgServiceChart extends Chart {

    private static final int SERVICE_PORT = 80;
    private static final int TARGET_PORT = 8080;
    public static final String NAMESPACE = "nodezoo";
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public PkgServiceChart(@NotNull Construct scope,
                           @NotNull String id,
                           @NotNull PkgServiceChartProps pkgServiceChartProps,
                           @Nullable ChartProps props) {
        super(scope, id, props);

        final String serviceName = MessageFormat.format("{0}-{1}-service", NAMESPACE, id);
        final var serviceManifestProps = new ServiceManifest.Props(SERVICE_PORT, TARGET_PORT,
                pkgServiceChartProps.isEks(), id, serviceName);
        final var serviceManifest = new ServiceManifest(this, serviceManifestProps);

        final var secretManifestName = pkgServiceChartProps.getSecretManifestProps() != null ?
                new ImagePullSecretManifest(this, pkgServiceChartProps.getSecretManifestProps()).getSecretName() :
                null;

        final var deploymentManifestProps = new DeploymentManifest.Props(
                TARGET_PORT,
                pkgServiceChartProps.getReplicas(),
                serviceManifest.getSelector(),
                secretManifestName,
                pkgServiceChartProps.getImageTag()
        );
        new DeploymentManifest(this, id, deploymentManifestProps);

        if (pkgServiceChartProps.isEks()) {
            final var ingressProps = IngressPropsBuilder.buildAlb(
                    serviceManifest.getService().getName(),
                    SERVICE_PORT);
            new IngressManifest(this, id, ingressProps);
        }
    }

    public PkgServiceChart(@NotNull Construct scope,
                           @NotNull String id,
                           @NotNull PkgServiceChartProps npmServiceChartProps) {

        this(scope, id, npmServiceChartProps, ChartProps.builder().namespace(NAMESPACE).build());
    }
}
