FROM openjdk:11.0.12-bullseye

ARG PROJECT_PATH

RUN apt-get update && \
    apt-get -y upgrade && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /builds/$PROJECT_PATH

COPY gradlew.* gradlew /builds/$PROJECT_PATH/
COPY gradle/ /builds/$PROJECT_PATH/gradle/
RUN  ./gradlew build -x test --no-daemon || true

COPY Makefile /builds/$PROJECT_PATH/

CMD ["bash"]
